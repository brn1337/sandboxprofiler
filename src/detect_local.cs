﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace SandboxProfiler
{
    public partial class Program
    {


        public static string detect_domainjoin()
        {
            try
            {
                ManagementObject ComputerSystem;
                using (ComputerSystem = new ManagementObject(String.Format("Win32_ComputerSystem.Name='{0}'", Environment.MachineName)))
                {
                    ComputerSystem.Get();
                    UInt16 DomainRole = (UInt16)ComputerSystem["DomainRole"];
                    return (DomainRole != 0 & DomainRole != 2).ToString();
                }
            }
            catch (System.Management.ManagementException) { return false.ToString(); }
            catch (Exception e) { return "#Error# " + dbg(e); }
        }

        public static string detect_drive()
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                foreach (DriveInfo drive in DriveInfo.GetDrives())
                {
                    try { 
                        if (drive.IsReady)
                        {
                            sb.AppendLine("   " + drive.Name + " " + drive.DriveType + " " + drive.VolumeLabel + " (" + SizeSuffix(drive.TotalFreeSpace) + "/" + SizeSuffix(drive.TotalSize) + ")");
                        }
                        else
                        {
                            sb.AppendLine("   " + drive.Name + " " + drive.DriveType + " [!] NotReady");
                        }
                    }
                    catch (Exception e)
                    {
                        sb.AppendLine("#Error# InnerFunc " + dbg(e));
                    }
                }
            }
            catch (Exception e)
            {
                sb.AppendLine("#Error# OutFunc " + dbg(e));
            }
            return sb.ToString();
        }

        // https://stackoverflow.com/questions/22205506/reading-digitalproductid-from-registry-comes-back-null
        public static string detect_winprodkey(string from = @"SOFTWARE\Microsoft\Windows NT\CurrentVersion", string valueName = "DigitalProductId")
        {
            RegistryKey hive = null;
            RegistryKey key = null;
            try
            {
                string possible_chars = "BCDFGHJKMPQRTVWXY2346789";
                var result = string.Empty;
                hive = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, Environment.MachineName);
                key = hive.OpenSubKey(from, false);
                var k = RegistryValueKind.Unknown;
                try { k = key.GetValueKind(valueName); }
                catch (Exception) { }

                if (k == RegistryValueKind.Unknown)
                {
                    key.Close();
                    hive.Close();
                    hive = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                    key = hive.OpenSubKey(from, false);
                    try { k = key.GetValueKind(valueName); }
                    catch (Exception) { }
                }

                if (k == RegistryValueKind.Binary)
                {
                    var pivot = 0;
                    var bytes = (byte[])key.GetValue(valueName);
                    var ints = new int[16];
                    for (var i = 52; i < 67; ++i) ints[i - 52] = bytes[i];
                    for (var i = 0; i < 25; ++i)
                    {
                        pivot = 0;
                        for (var j = 14; j >= 0; --j)
                        {
                            pivot <<= 8;
                            pivot ^= ints[j];
                            ints[j] = ((int)Math.Truncate(pivot / 24.0));
                            pivot %= 24;
                        }
                        result = possible_chars[pivot] + result;
                        if ((i % 5 == 4) && (i != 24))
                        {
                            result = "-" + result;
                        }
                    }
                }
                return result;
            }
            catch (Exception) { return null; }
            finally
            {
                key?.Close();
                hive?.Close();
            }
        }
        public static string detect_offprodkey()
        {
            try
            {
                string registry_key = @"SOFTWARE\Microsoft\Windows NT\CurrentVersion\SoftwareProtectionPlatform";
                using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
                {
                    return (string)key.GetValue("BackupProductKeyDefault");
                }
            }
            catch (Exception e) { return "#Error# " + dbg(e); }

        }

        public static string detect_env()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                foreach (DictionaryEntry e in System.Environment.GetEnvironmentVariables())
                {
                    sb.AppendLine(" " + e.Key + ": " + e.Value);
                }
                return sb.ToString();
            }
            catch (Exception e) { return "#Error# " + dbg(e); }

        }

        // to be fixed
        public static string _detect_software_legacy()
        {
            StringBuilder sb = new StringBuilder();
            try{
                ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_Product");
                foreach (ManagementObject mo in mos.Get())
                {
                    Console.WriteLine(mo["Name"]);
                    Console.WriteLine(mo["Publisher"]);
                }
            }catch (Exception e) { return "#Error# Legacy " + dbg(e); }
            return sb.ToString();
        }

        public static StringBuilder _detect_software(Microsoft.Win32.RegistryKey key)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string subkey_name in key.GetSubKeyNames())
            {
                using (RegistryKey subkey = key.OpenSubKey(subkey_name))
                {
                    if (subkey.GetValueNames().Contains("Publisher"))
                    {
                        string n = (string)subkey.GetValue("Publisher");
                        if (!n.StartsWith("Microsoft"))
                        {
                            if (subkey.GetValue("DisplayVersion") == null)
                            {
                                sb.AppendLine(" " + n);
                            }
                            else
                            {
                                sb.AppendLine(" " + subkey.GetValue("DisplayName") + ": " + subkey.GetValue("DisplayVersion"));
                            }
                        }
                    }
                }
            }
            return sb;
        }


        public static string detect_software()
        {
            StringBuilder sb = new StringBuilder();
            string registry_key = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            try
            {
                using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
                {
                    StringBuilder localmachine = _detect_software(key);
                    sb.Append(localmachine);
                }
            }
            catch (System.NullReferenceException) { sb.AppendLine(" [!] No entries for LocalMachine"); }
            catch (Exception e) { sb.AppendLine(" #Error# LocalMachine " + dbg(e)); }

            try
            {
                using (Microsoft.Win32.RegistryKey key = Registry.CurrentUser.OpenSubKey(registry_key))
                {
                    StringBuilder currentuser = _detect_software(key);
                    sb.Append(currentuser);
                }
            }
            catch (System.NullReferenceException) { sb.AppendLine(" [!] No entries for CurrentUser"); }
            catch (Exception e) { sb.AppendLine(" #Error# CurrentUser " + dbg(e)); }

            /**
            if (error > 0)
            {
                sb.AppendLine("### Legacy software start ###");
                sb.Append(_detect_software_legacy());
                sb.AppendLine("### Legacy software end ###");
            }
            **/

            return sb.ToString();

        }

        public static string detect_cpu()
        {
            try
            {
                string registry_key = @"HARDWARE\DESCRIPTION\System\CentralProcessor\0";
                using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
                {
                    return (string)key.GetValue("Identifier");
                }
            }
            catch (Exception e) { return "#Error# " + dbg(e); }

        }






        public static string detect_proxy()
        {
            try
            {
                string registry_key = @"Software\Microsoft\Windows\CurrentVersion\Internet Settings";
                using (Microsoft.Win32.RegistryKey key = Registry.CurrentUser.OpenSubKey(registry_key))
                {
                    if ((int)key.GetValue("ProxyEnable") == 1)
                    {
                        return "Enabled " + (string)key.GetValue("ProxyServer");
                    }
                    else if ((int)key.GetValue("ProxyEnable") == 0)
                    {
                        return "Disabled " + (string)key.GetValue("ProxyServer");
                    }
                    else
                    {
                        return "None";
                    }
                }
            }
            catch (Exception e) { return "#Error# " + dbg(e); }

        }

        public static string detect_process()
        {
            StringBuilder sb = new StringBuilder();
            Process[] processlist = Process.GetProcesses();
            var allproc = new List<string> { };
            foreach (Process theprocess in processlist)
            {
                try
                {
                    if (theprocess.MainModule.FileVersionInfo.CompanyName.StartsWith("Microsoft") || theprocess.MainModule.FileName == System.Reflection.Assembly.GetExecutingAssembly().Location)
                    {
                        // skipping process
                    }
                    else
                    {
                        allproc.Add("[" + theprocess.MainModule.FileVersionInfo.CompanyName + "] " + theprocess.MainModule.FileName);
                    }
                }
                catch
                {
                    //Console.WriteLine("FAIL Process: {0} ", theprocess.ProcessName);
                }
            }
            var uniqproc = allproc.Distinct().ToList();
            var uniqsortproc = uniqproc.OrderBy(q => q).ToList();
            foreach (string p in uniqsortproc)
            {
                sb.AppendLine(" " + p);
            }

            return sb.ToString();
            //return "#Error#";
        }
        public static string detect_os()
        {
            //Get Operating system information.
            OperatingSystem os = Environment.OSVersion;
            //Get version information about the os.
            Version vs = os.Version;

            //Variable to hold our return value
            string operatingSystem = "";

            if (os.Platform == PlatformID.Win32Windows)
            {
                //This is a pre-NT version of Windows
                switch (vs.Minor)
                {
                    case 0:
                        operatingSystem = "95";
                        break;
                    case 10:
                        if (vs.Revision.ToString() == "2222A")
                            operatingSystem = "98SE";
                        else
                            operatingSystem = "98";
                        break;
                    case 90:
                        operatingSystem = "Me";
                        break;
                    default:
                        break;
                }
            }
            else if (os.Platform == PlatformID.Win32NT)
            {
                switch (vs.Major)
                {
                    case 3:
                        operatingSystem = "NT 3.51";
                        break;
                    case 4:
                        operatingSystem = "NT 4.0";
                        break;
                    case 5:
                        if (vs.Minor == 0)
                            operatingSystem = "2000";
                        else
                            operatingSystem = "XP";
                        break;
                    case 6:
                        if (vs.Minor == 0)
                            operatingSystem = "Vista";
                        else if (vs.Minor == 1)
                            operatingSystem = "7";
                        else if (vs.Minor == 2)
                            operatingSystem = "8";
                        else
                            operatingSystem = "8.1";
                        break;
                    case 10:
                        if (vs.Build >= 22000)
                            operatingSystem = "11";
                        else
                            operatingSystem = "10";
                        break;
                    default:
                        break;
                }
            }
            //Make sure we actually got something in our OS check
            //We don't want to just return " Service Pack 2" or " 32-bit"
            //That information is useless without the OS version.
            if (operatingSystem != "")
            {
                //Got something.  Let's prepend "Windows" and get more info.
                operatingSystem = "Windows " + operatingSystem;
                //See if there's a service pack installed.
                if (os.ServicePack != "")
                {
                    //Append it to the OS name.  i.e. "Windows XP Service Pack 3"
                    operatingSystem += " " + os.ServicePack;
                }
                //Append the OS architecture.  i.e. "Windows XP Service Pack 3 32-bit"
                string arch = "";
                if (Environment.Is64BitOperatingSystem == true)
                    arch = "64";
                else
                    arch = "32";

                operatingSystem += " " + arch + "-bit";

                //Append the exact version
                operatingSystem += " (" + vs.Major + "." + vs.Minor + "." + vs.Build + ")";
            }
            //Return the information we've gathered.
            return operatingSystem;
        }

        public static string detect_fqdn()
        {
            try
            {
                string domainName = IPGlobalProperties.GetIPGlobalProperties().DomainName;
                string hostName = Dns.GetHostName();
                domainName = "." + domainName;
                if (!hostName.EndsWith(domainName))
                {
                    hostName += domainName;
                }
                return hostName;
            }
            catch (Exception e) { return "#Error# " + dbg(e); }

        }


        public static string getOSArchitecture()
        {
            if (Environment.Is64BitOperatingSystem == true)
            {
                return "64";
            }
            else
            {
                return "32";
            }
        }


    }
}
