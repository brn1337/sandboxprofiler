﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.DirectoryServices;
using System.IO;
using System.Text;

namespace SandboxProfiler
{
    public partial class Program
    {

        public static string detect_wallpaper()
        {
            try
            {
                string registry_key = @"Control Panel\Desktop";
                using (Microsoft.Win32.RegistryKey key = Registry.CurrentUser.OpenSubKey(registry_key))
                {
                    try { return (string)key.GetValue("Wallpaper"); } catch { }
                }
            }
            catch (Exception e) { return "#Error# " + dbg(e); }
            return "#Error#";
        }
        public static string detect_recentfile()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Recent);
                var files = Directory.EnumerateFiles(path);
                int count = 0;
                int t = 0;
                foreach (var x in Directory.EnumerateFiles(path))
                {
                    t++;
                }
                sb.AppendLine(" (" + t + ")");
                foreach (var e in files)
                {
                    if (count > 20)
                        break;
                    if (System.IO.File.Exists(e))
                    {
                        IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
                        try
                        {
                            IWshRuntimeLibrary.IWshShortcut link = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(e);
                            if (!String.IsNullOrEmpty(link.TargetPath))
                                sb.AppendLine(" " + link.TargetPath);
                            count++;
                        }
                        catch (System.Runtime.InteropServices.COMException) { }
                    }
                }
                return sb.ToString();
            }
            catch (Exception e) { return "#Error# " + dbg(e); }

        }
        public static string detect_lastlog()
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DirectoryEntry dirs = new DirectoryEntry("WinNT://" + Environment.MachineName);
                foreach (DirectoryEntry de in dirs.Children)
                {
                    if (de.SchemaClassName == "User")
                    {
                        //Console.WriteLine(de.Name);
                        if (de.Properties["lastlogin"].Value != null)
                        {
                            sb.AppendLine("   Login  " + de.Name + ": " + de.Properties["lastlogin"].Value.ToString());
                        }
                        if (de.Properties["lastlogoff"].Value != null)
                        {
                            sb.AppendLine("   Logoff " + de.Name + ": " + de.Properties["lastlogoff"].Value.ToString());
                        }
                    }
                }
            }
            catch (Exception e) { return "#Error# " + dbg(e); }
            return sb.ToString();
        }
        public static string check_uptime()
        {
            try
            {
                //https://stackoverflow.com/a/34010110
                var start = Stopwatch.StartNew();
                var ticks = Stopwatch.GetTimestamp();
                var uptime = ((double)ticks) / Stopwatch.Frequency;
                var uptimeTimeSpan = TimeSpan.FromSeconds(uptime);
                return uptimeTimeSpan.Subtract(start.Elapsed).ToString(@"dd\d\a\y\s\ hh\h\o\u\r\s\ mm\m\i\n\u\t\e\s\ ss\s\e\c\o\n\d\s");
            }
            catch (Exception e) { return "#Error# " + dbg(e); }

        }



    }
}
