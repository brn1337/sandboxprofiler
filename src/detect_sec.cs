﻿using Microsoft.TeamFoundation.Common;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace SandboxProfiler
{
    public partial class Program
    {

        public static string detect_dll()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("");
            bool first = true;
            try
            {
                Process currentProcess = Process.GetCurrentProcess();
                foreach (ProcessModule module in currentProcess.Modules)
                {
                    if (!((module.FileVersionInfo.ProductName.StartsWith("Microsoft")) || (module.FileName.ToLower().EndsWith(".exe"))))
                    {
                        if (first)
                        {
                            sb.AppendLine("");
                            first = false;
                        }
                        sb.AppendLine("   " + module.FileName + ": " + module.FileVersionInfo.ProductName);
                    }
                }
                if (sb.ToString() == "") 
                    return ": [!] None";
                return sb.ToString();
            }
            catch (Exception e) { return "   #Error# " + dbg(e); }

        }

        public static string detect_admingroup()
        {
            try
            {
                WindowsIdentity identity = WindowsIdentity.GetCurrent();
                if (identity != null)
                {
                    WindowsPrincipal principal = new WindowsPrincipal(identity);
                    List<Claim> list = new List<Claim>(principal.UserClaims);
                    Claim c = list.Find(p => p.Value.Contains("S-1-5-32-544"));
                    if (c != null)
                        return true.ToString();
                }
                return false.ToString();
            }
            catch (Exception e) { return "   #Error# " + dbg(e); }
        }

        public static string detect_adminpriv()
        {
            try
            {
                WindowsIdentity identity = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(identity);
                bool res = principal.IsInRole(WindowsBuiltInRole.Administrator);
                return res.ToString();
            }
            catch (Exception e) { return "   #Error# " + dbg(e); }

        }

        public static string detect_uac()
        {
            try
            {
                string UAC_key = @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System";
                object i = Registry.GetValue(UAC_key, "EnableLUA", -1);
                if ((int)i == 0)
                {
                    return "Disabled";
                }
                else if ((int)i == 1)
                {
                    return "Enabled";
                }
            }
            catch (Exception e) { return "#Error# " + dbg(e); }
            return "#Error#";
        }

        public static string detect_amsi()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                string registry_key = @"SOFTWARE\Microsoft\AMSI\Providers";
                using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
                {
                    foreach (string subkey_name in key.GetSubKeyNames())
                    {
                        using (RegistryKey subkey = key.OpenSubKey(subkey_name))
                        {
                            if (subkey.GetValueNames().Contains("(Default)"))
                            {
                                sb.Append((string)subkey.GetValue("(Default)") + ", ");
                            }
                        }
                    }
                }
                if (sb.ToString() == "") { sb.Append("[!] None"); }
                return sb.ToString();
            }
            catch (Exception e) { return "#Error# " + dbg(e); }

        }

        public static string detect_applocker()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                string registry_key = @"Software\Policies\Microsoft\Windows\SrpV2";
                using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
                {
                    foreach (string category in key.GetSubKeyNames())
                    {
                        using (RegistryKey cat = key.OpenSubKey(category))
                        {
                            if (cat.GetValueNames().Contains("EnforcementMode"))
                            {
                                if ((int)cat.GetValue("EnforcementMode") == 1)
                                {
                                    sb.Append("   Applocker enforcement for " + category + " with " + cat.GetSubKeyNames().Count() + " rules");
                                    /**
                                    foreach (string policy in cat.GetSubKeyNames())
                                    {
                                        using (RegistryKey pol = cat.OpenSubKey(policy))
                                        {
                                            sb.Append("    " + pol.GetValue("Value"));
                                        }
                                    }
                                    **/
                                }
                            }
                        }
                    }
                }
                if (sb.ToString() == "") { sb.Append(": [!] None"); }
                return sb.ToString();
            }
            catch (System.NullReferenceException) { return ": [!] None"; }
            catch (Exception e) { return "#Error# " + dbg(e); }
        }
        public static string detect_firewall()
        {
            try
            {
                Type NetFwMgrType = Type.GetTypeFromProgID("HNetCfg.FwMgr", false);
                INetFwMgr mgr = (INetFwMgr)Activator.CreateInstance(NetFwMgrType);
                bool firewallEnabled = mgr.LocalPolicy.CurrentProfile.FirewallEnabled;
                /**
                Type tNetFwPolicy2 = Type.GetTypeFromProgID("HNetCfg.FwPolicy2");
                dynamic fwPolicy2 = Activator.CreateInstance(tNetFwPolicy2) as dynamic;
                IEnumerable Rules = fwPolicy2.Rules as IEnumerable;
                foreach (dynamic rule in Rules)
                {
                    Console.WriteLine(rule.Name);
                }
                **/
                return firewallEnabled.ToString();
            }
            catch (System.Runtime.InteropServices.COMException) { return "No"; }
            catch (Exception e) { return "#Error# " + dbg(e); }

        }


    }
}
