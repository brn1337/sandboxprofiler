﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace SandboxProfiler
{
    public partial class Program
    {

        private class MyWebClient : WebClient
        {
            protected override WebRequest GetWebRequest(Uri uri)
            {
                WebRequest w = base.GetWebRequest(uri);
                w.Timeout = 5 * 1000;
                return w;
            }
        }

        public static bool detect_ipv4()
        {
            try
            {
                WebClient webclient = new WebClient();
                webclient.Proxy = System.Net.WebRequest.DefaultWebProxy;
                webclient.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                string data = webclient.DownloadString(internet_url);
                if (data.Contains(internet_body))
                {
                    return true;
                }
                return false;
            }
            catch { }
            return false;
        }

        public static void detect_dns()
        {

            if (string.IsNullOrEmpty(dns_primary))
            {
                try { Dns.GetHostAddresses(dns_primary); } catch { }
            }
            
            if (string.IsNullOrEmpty(dns_secondary))
            {
                try { Dns.GetHostAddresses(dns_secondary); } catch { }
            }

        }

        public static string detect_ipv6()
        {
            try
            {
                ServicePointManager
                    .ServerCertificateValidationCallback +=
                    (sender, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                WebClient webclient = new WebClient();
                webclient.Proxy = System.Net.WebRequest.DefaultWebProxy;
                webclient.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                string data = webclient.DownloadString(ipv6_url);
                if (data.Contains(ipv6_body))
                {
                    string toBeSearched = ipv6_body;
                    int ix = data.IndexOf(toBeSearched);

                    if (ix != -1)
                    {
                        string code = data.Substring(ix + toBeSearched.Length).Substring(2, 31);
                        return code;
                    }
                }
                return false.ToString();
            }
            catch { }
            return false.ToString();
        }

        


        public static string detect_netadapter()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                List<String> values = new List<String>();
                foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    IPInterfaceProperties properties = nic.GetIPProperties();
                    string versions = "";
                    if (nic.Supports(NetworkInterfaceComponent.IPv4))
                    {
                        versions = "IPv4";
                    }
                    if (nic.Supports(NetworkInterfaceComponent.IPv6))
                    {
                        if (versions.Length > 0)
                        {
                            versions += " ";
                        }
                        versions += "IPv6";
                    }
                    sb.AppendLine(" [" + nic.OperationalStatus + "] " + nic.NetworkInterfaceType + ": " + nic.Name + " (" + versions + ")");
                    foreach (UnicastIPAddressInformation ip in nic.GetIPProperties().UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            sb.AppendLine("   IP: " + ip.Address.ToString() + " (" + ip.IPv4Mask.ToString() + ")");
                        }
                    }
                    if (!string.IsNullOrEmpty(properties.DnsSuffix))
                    {
                        sb.AppendLine("   DNS: " + properties.DnsSuffix);
                    }
                }
                return sb.ToString();
            }
            catch (Exception e) { return "#Error# " + dbg(e); }

        }

        public static string detect_dcconn()
        {
            try
            {
                const string RootDsePath = "LD" + "AP://Roo" + "tDSE";
                DirectoryEntry rootDse = new DirectoryEntry(RootDsePath) { AuthenticationType = AuthenticationTypes.Secure };
                string domain_name = System.DirectoryServices.ActiveDirectory.Domain.GetComputerDomain().Name;
                return domain_name;
            }
            catch (System.DirectoryServices.ActiveDirectory.ActiveDirectoryObjectNotFoundException)
            {
                return "Unreachable";
            }
        }

        public static string detect_time()
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                WebClient webclient = new WebClient();
                webclient.Proxy = System.Net.WebRequest.DefaultWebProxy;
                webclient.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                string s = webclient.DownloadString(timeapi_url);
                JObject json = JObject.Parse(s);
                return json["year"].ToString()+"/" + string.Format("{0:00}", json["month"]) + "/" + string.Format("{0:00}", json["day"]) + " " + json["time"].ToString() + " " + json["timeZone"].ToString();
            }
            catch (System.Net.WebException) { return "[!] Unreachable"; }
            catch (Exception e) { return "#Error# " + dbg(e); }
        }


        public static string detect_tor()
        {
            try
            {

                ServicePointManager
                    .ServerCertificateValidationCallback +=
                    (sender, cert, chain, sslPolicyErrors) => true; 
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                string urlString = $"https://check.torproject.org/api/ip";
                WebClient webclient = new WebClient();
                webclient.Proxy = System.Net.WebRequest.DefaultWebProxy;
                webclient.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                string s = webclient.DownloadString(urlString);
                return s;
            }
            catch (System.Net.WebException) { return "[!] Unreachable"; }
            catch (Exception e) { return "#Error# " + dbg(e); }

        }
        public static string detect_ip()
        {
            try
            {
                ServicePointManager
                    .ServerCertificateValidationCallback +=
                    (sender, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                string urlString = $"https://api.myip.com/";
                WebClient webclient = new WebClient();
                webclient.Proxy = System.Net.WebRequest.DefaultWebProxy;
                webclient.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                string s = webclient.DownloadString(urlString);
                return s;
            }
            catch (System.Net.WebException) { return "[!] Unreachable"; }
            catch (Exception e) { return "#Error# " + dbg(e); }

        }


        public static string detect_ssl()
        {
            string urlString = ssl_url;
            try
            {
                WebClient webclient = new WebClient();
                webclient.Proxy = System.Net.WebRequest.DefaultWebProxy;
                webclient.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                webclient.DownloadString(urlString);
            }
            catch
            {
                return "DETECTED";
            }
            return "No";
        }


    }
}
