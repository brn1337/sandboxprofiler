# SandboxProfiler

Collect information of internet-connected sandboxes, no backend needed.
This is achieved using telegram and interact.sh to collect data, however custom listeners are also supported.
Non internet-connected sandboxes are not in scope, however it is possible to check for DNS exfiltration.

## Why

There are numerous techniques to detect a VM/Sandbox. 
One of the best collection of techniques I know is [evasions.checkpoint.com](https://evasions.checkpoint.com/).
There are also great implementations of these techniques:

- https://github.com/LordNoteworthy/al-khaser
- https://github.com/CheckPointSW/InviZzzible
- https://github.com/a0rtega/pafish
- https://github.com/Back-X/anti-vm

Apparently most malware implement 1/2 of these techniques, as shown [here](https://mdpi-res.com/d_attachment/jcp/jcp-01-00003/article_deploy/jcp-01-00003.pdf) (pag. 30).
Adding these checks without proper testing will do more harm then good, as shown by [this](https://www.trustedsec.com/blog/enumerating-anti-sandboxing-techniques/) article, because evasion techniques are themself flagged as malicious.

This approach is usually a shot in the dark and can be very frustrating, let's shine light on these sandboxes.

## Avoid sandbox fingerprint

TLDR; You can't avoid sandbox fingerprint reliably

The easy way to counter sandbox fingerprint is to block any internet connectivity (including DNS), however without that many malware will not start.
Another way is to block the telegram endpoints, however it is possible to use a custom url to send data.
Detecting the data in transit is also useless as it can be easily encrypted/encoded.

# Getting started

## Collect information

1. Create a Telegram bot
2. Create a DNS domain with interact.sh
3. Edit configuration in `Program.cs`
   - Set `production = true`
   - Set `use_telegram = true`
   - Set telegram token and chatid
   - Set dns_primary to the interactsh hostname
4. Compile using Visual Studio
5. Upload/run the .exe to your favourite sandbox
6. Check your telegram chat!

## Analyze information

1. From telegram desktop download the chat data as json
2. Parse the .json using `parser.py`
3. Print statistics using `statistics.ps1`

# Statistics

Below some interesting findings during my tests. 
The following results were gathered from uploading it to 4 public sandbox services, keep in mind that any service can run the same payload multiple times and distribute it to third-parties.

```
Files analyzed:  50

############################
Resolution
 1280 x 1024                                 37
 1024 x 768                                  5
 1280 x 720                                  4
 800 x 600                                   3
 1280 x 800                                  1

############################
System proxy
 Disabled                                    48
 Enabled extractor.proxy:8080                2

############################
SSL interception
 No                                          48
 Yes                                         2

############################
Software
 LibreOffice 6.4.4.2: 6.4.4.2                2
 Google Chrome: 77.0.3865.90                 1
 Mozilla Firefox 75.0 (x64 en-US): 75.0      2
 Mozilla Firefox 73.0.1 (x64 de): 73.0.1     2
 Mozilla Firefox 74.0 (x86 en-US): 74.0      1
 Mozilla Firefox 61.0 (x86 en-US): 61.0      1
 Kaspersky Internet Security: 19.0.0.1088    2

############################
Processes running
 C:\Windows\Sysmon64.exe                     36
 C:\Windows\sysmon.exe                       2
 C:\Program Files (x86)\AutoIt3\AutoIt3.exe  1

############################
CPU 
 1 core                                      34
 2 cores                                     11
 4 cores                                     5

############################
User
 WIN-5E07COS9ALR\Abby                        34
 work\admin                                  2
 DESKTOP-B0T93D6\george                      2
 DESKTOP-547Q8VP\tim                         2
 WALKER-PC\WALKER                            2
 DESKTOP-D019GDM\Frank                       2
 HAPUBWS-PC\HAPUBWS                          1
 DWHRNUUP\Admin                              1
 ANNA-PC\Anna                                1
 FSHLRPTB\Admin                              1
 DESKTOP-G9MZN9J\4LkDtwDxf                   1
 0CC47AC83803\Administrator                  1
```

Some interesting discoveries:

- Triggering a BSOD will cause some sandboxes not to generate a report
- Out of all the sandboxes which run offline (unknown number, obviously) some are not protected against dns exfiltration
- Some sandboxes are not running a geniune version of Windows/Office
- Some sandboxes have their system date still in 2021

# Other information

The whole code is wrapped in lots of try/catch, in order to send data even if everything breaks.

The binary has a low detection rate, which shouldn't be an issue, unless a sandbox is used only on 0 detection binaries. I know of no such thing, however it is possible.

The binary can achieve a 0 detection rate avoiding Telegram and some of the checks implemented, such as:

- CPU model
- Uptime
- Network adapters
- IP reputation
- Tor detection

# Future Improvements

- Strong file exfiltration (currently only working with small text files)
- DNS exfiltration (currently only ping)
- Network awareness

# Output example 

Mix of real data, ip has been changed

```
Collect v0.17|dummysandbox|7189FAF3

####################
OS/User/Config
 HAPUBWS-PC\HAPUBWS
 Domain joined: False
 Machine: HAPUBWS-PC.
 CWD: C:\
 Executable: C:\profiler.exe
 OS: Windows 10 64-bit (10.0.15063)
 Uptime: 00days 00hours 17minutes 33seconds
 Language/Keyboard: en-US/US
 Wallpaper: C:\Windows\web\wallpaper\Windows\img0.jpg
 Windows product key: BBBBB-BBBBB-BBBBB-BBBBB-BBBBB
 Office  product key: 
 Real date: 2022/06/16 01:33 Europe/Rome
 Sys date:  2022/06/15 23:44 GMT+02:00
 Login/logoff
   Login  Administrator: 11/21/2010 5:47:20 AM
   Login  HAPUBWS: 6/15/2022 11:44:24 PM

####################
Hardware
 CPU Model: Intel64 Family 6 Model 79 Stepping 0 (#4 core)
 RAM: 4096
1024 x 768
 Drives
   C:\ Fixed  (24 GB/100 GB)
   D:\ CDRom [!] NotReady
   Z:\ Fixed  (24 GB/100 GB)

####################
Security
 Admin: group=True effective=True
 UAC: Disabled
 AMSI providers: [!] None
 Non-Microsoft attached DLLs:
   C:\Windows\system32\OLEAUT32.dll
 AppLocker: [!] None

####################
Network
 IPv6: False
 Firewall: True
 DC connectivity: Unreachable
 SSL interception: No
 System proxy: Enabled extractor.proxy:8080
 IP Reputation: {"ip":"64.234.23.162","country":"Canada","cc":"CA"}
 Tor: [!] Unreachable
 [Up] Ethernet: Local Area Connection (IPv4 IPv6)
   IP: 192.168.242.31 (255.255.252.0)
   DNS: scl3.dc
 [Up] Loopback: Loopback Pseudo-Interface 1 (IPv4 IPv6)
   IP: 127.0.0.1 (255.0.0.0)
 [Down] Tunnel: isatap.scl3.dc (IPv6)
   DNS: scl3.dc

####################
Software
 7-Zip 19.00 (x64): 19.00
 Mozilla Firefox 75.0 (x64 en-US): 75.0
 Mozilla Maintenance Service: 75.0
 VLC media player: 3.0.6
 Java 8 Update 66 (64-bit): 8.0.660.17
 Java SE Development Kit 8 Update 66 (64-bit): 8.0.660.17

####################
Processes running (excluding Microsoft)
 [AutoIt Team] C:\Program Files (x86)\AutoIt3\AutoIt3.exe

####################
Recent files (6)
 C:\Users\HAPUBWS\Documents\Database1.accdb
 C:\Users\fdaUF0L\Downloads\VxFuzzer-b7753aaeab5c87f441281cafa2cb8c70ade0420b\fastfuzzer.au3
 C:\fuzzer_winupdt.log
 C:\Users\fdaUF0L\Desktop\VxFuzzer\slowfuzzer.au3
 C:\Users\fdaUF0L\Downloads\VxFuzzer-b7753aaeab5c87f441281cafa2cb8c70ade0420b
 C:\Users\fdaUF0L\Desktop\VxFuzzer
 ```

# License

SandboxProfiler is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1)

```
CC-BY-NC-SA This license requires that reusers give credit to the creator.
It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, for noncommercial purposes only.
If others modify or adapt the material, they must license the modified material under identical terms.
- Credit must be given to you, the creator.
- Only noncommercial use of your work is permitted.
- Adaptations must be shared under the same terms.
```
