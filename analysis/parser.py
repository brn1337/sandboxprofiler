import json
import os


## CONFIGURATION START ##
dest_dir = 'splitted'
telegram_json = 'result.json'
bot_username = 'profiler'
## CONFIGURATION END ##


# Create dir if missing
if not os.path.exists(dest_dir):
    os.makedirs(dest_dir)

with open(telegram_json, encoding = 'utf-8', mode = 'r') as f:
    c = f.read()
    data = json.loads(c)
    for i in data['messages']:
        # Iterate only messages from bot
        if 'from' in i and i['from'] == bot_username:
            tmp = ""
            print(i['id'])
            for x in i['text']:
                if isinstance(x, str):
                    tmp += x
                    print(x)
                # Links are treated as json entities instead of text by TG
                else:
                    print(x['text'])
                    tmp += x['text']
            
            # Write message to file
            dest_file = '%s\%s' % (dest_dir, i['id'])
            with open(dest_file, 'w', encoding = 'utf-8') as f:
                f.write(tmp)
